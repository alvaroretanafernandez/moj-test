const LineByLineReader = require('line-by-line');
const _ = require('lodash');

/**
 * Line reader
 * @type {LineByLineReader}
 */
lr = new LineByLineReader('1.txt');
/**
 * Array item
 * @type {Array}
 */
let arr = [];

function printText(p, name) {
    if (p[name]) {
        let reviewer = p[name];
        const isNotReviewee = _.find(p, (it) => {
            return it.reviewee === reviewer
        }) === undefined;
        let mark = isNotReviewee ? '' : '|';
        console.log(mark + reviewer[0].reviewer);
        reviewer.forEach(rev => {
            const reviewees = p[rev.reviewee];
            if (reviewees) {
                console.log('|' + rev.reviewee);
                reviewees.forEach(xx => {
                    const secreviewees = p[xx.reviewee];
                    if (secreviewees) {
                        console.log('||' + xx.reviewee);
                        const trreviewees = p[xx.reviewee];
                        if (trreviewees) {
                            trreviewees.forEach(dd => {
                                if (p[dd.reviewee]) {
                                    console.log('|||' + dd.reviewee);
                                    const frreviewees = p[dd.reviewee];
                                    if (frreviewees) {
                                        frreviewees.forEach(ss => {
                                            if (p[ss.reviewee]) {
                                                console.log('||||' + ss.reviewee);
                                            } else {
                                                console.log('||||/-' + ss.reviewee);
                                            }
                                        })
                                    } else {
                                        console.log('|||/-' + dd.reviewee);
                                    }
                                }
                                else {
                                    console.log('|||/-' + dd.reviewee);
                                }
                            })
                        } else {
                            console.log('||/-' + xx.reviewee);
                        }
                    } else {
                        console.log('||/-' + xx.reviewee);
                    }
                })
            } else {
                console.log('|/-' + rev.reviewee);
            }
        });
    }
}

lr.on('line', function (line) {
    const splitted = line.split(' reviews ');
    arr.push({ reviewer: splitted[0], reviewee: splitted[1] });
});

lr.on('end', function () {
    const p = _.groupBy(arr, 'reviewer');
    printText(p, arr[0].reviewer);

});
